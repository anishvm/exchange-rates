
# Get Current Exchange Rate of Czech Koruna

## Table of Contents
- [Overview](#overview)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
  - [Code Quality](#code-quality)
  - [Building and Pushing Docker Image](#building-and-pushing-docker-image)
  - [Running Tests](#running-tests)
  - [Deployment to AWS ECS](#deployment-to-aws-ecs)
- [Contributing](#contributing)
- [License](#license)

## Overview
This project aims to provide the current exchange rate of Czech Koruna against US dollar. The rates are updated every hour.
A lambda function is used to fetch the rates and store in s3. A python flask app is used to display the data related to the CZK
in an HTML page. The HTML is served by a container running on AWS ECS behind an ALB. The infrastructure is deployed using Terraform.

## Prerequisites

## Getting Started
This repo contains the terraform code, lambda function code and the application code.
The Gitlab CI pipeline is used to build & push the docker image to docker hub and deploy the application stack to AWS Cloud.

### Building docker image
The pipeline will build and push the image to docker hub except for any changes in terraform branch.

### Deployment to AWS ECS
Once the container image is pushed to the repo checkout the terraform branch & update the image tag in the below path.
Commit the changes and the newer version will be rolled out to the ECS

```
terraform/terraform.tfvars
```

## Todos

- [ ] Implement semantic versioning.
- [ ] Update pipeline to push new image tag to repository.
- [ ] Create Terraform module for VPC
- [ ] Ensure that the README has necessary details.

