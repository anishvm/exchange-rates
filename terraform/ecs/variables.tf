variable "cluster_name" {}

variable "service_name" {}

variable "cpu" {}

variable "memory" {}

variable "container_definitions" {
  type = any
}

variable "bucket_arn" {}

variable "bucket" {}

variable "vpc_id" {}

variable "lb_subnets" {
  type = list(string)
}

variable "app_subnets" {
  type = list(string)
}