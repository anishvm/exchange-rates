data "aws_iam_policy_document" "assume_role_scheduler" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["scheduler.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "scheduler_role_policy" {
  statement {
    effect = "Allow"
    resources = [aws_lambda_function.this.arn]
    actions = ["lambda:InvokeFunction"]
  }
}

resource "aws_iam_role" "iam_for_scheduler" {
  name               = "${var.function_name}-lambda-scheduler-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_scheduler.json
}

resource "aws_iam_role_policy" "scheduler_policy" {
  name = "lambda_policy"
  role = aws_iam_role.iam_for_scheduler.id
  policy = data.aws_iam_policy_document.scheduler_role_policy.json
}


resource "aws_scheduler_schedule" "this" {
  name       = "${var.function_name}-scheduler"

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "rate(24 hours)"

  target {
    arn      = aws_lambda_function.this.arn
    role_arn = aws_iam_role.iam_for_scheduler.arn
  }
}