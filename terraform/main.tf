provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}

module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "app-vpc"
  cidr               = "10.0.0.0/16"
  azs                = ["eu-west-2a", "eu-west-2b"]
  public_subnets     = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets    = ["10.0.0.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  public_subnet_tags = {
    "Name" = "Public"
  }
  private_subnet_tags = {
    "Name" = "Priavte"
  }
}

module "s3" {
  source = "./s3"
  bucket = "exchange-rates"
}

module "lambda" {
  source        = "./lambda"
  bucket        = module.s3.bucket
  bucket_arn    = module.s3.bucket_arn
  function_name = "get-exchange-rates"
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.9"
  source_file   = "../lambda/lambda_function.py"
}

module "ecs" {
  source       = "./ecs"
  cluster_name = "exchange-rate-app-cluster"
  vpc_id       = module.vpc.vpc_id
  container_definitions = jsonencode([
    {
      name  = "app"
      image = var.image
      portMappings = [
        {
          containerPort = 5000
          hostPort      = 5000
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-create-group  = "true"
          awslogs-group         = "exchange-rates"
          awslogs-region        = "eu-west-2"
          awslogs-stream-prefix = "ecs"
        }
      }
      environment = [
        {
          name  = "bucket"
          value = module.s3.bucket
        }
      ]
    }
  ])
  cpu          = "256"
  memory       = "512"
  service_name = "exchange-rate"
  app_subnets  = module.vpc.private_subnets
  bucket       = module.s3.bucket
  bucket_arn   = module.s3.bucket_arn
  lb_subnets   = module.vpc.public_subnets
}
