terraform {
  backend "s3" {
    bucket         = "exchange-rate-app-833606799879"
    key            = "terraform.tfstate"
    region         = "eu-west-2"  # Adjust the region accordingly
    encrypt        = true
    dynamodb_table = "exchange-rate-app-833606799879"  # Optional: For state locking
    kms_key_id = "arn:aws:kms:eu-west-2:833606799879:key/ffa1ef11-44ad-4b41-8eef-e9362f25dec1"
    role_arn = "arn:aws:iam::833606799879:role/exchange-rate-app"
  }
}