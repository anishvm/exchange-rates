region = "eu-west-2"

default_tags = {
  environment = "prod"
}

image = "ansih/exchange-rate:latest"