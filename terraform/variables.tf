variable "region" {
  description = "AWS region"
  default     = "eu-west-2"
}

variable "default_tags" {
  type = map(string)
}

variable "image" {}