#!/bin/bash

CURRENT_VERSION=$1
BUMP_TYPE=$2

IFS='.' read -r -a VERSION_PARTS <<< "$CURRENT_VERSION"

case $BUMP_TYPE in
  major)
    ((VERSION_PARTS[1]++))
    VERSION_PARTS[2]=0
    ;;
  minor)
    ((VERSION_PARTS[2]++))
    ;;
  patch)
    ((VERSION_PARTS[3]++))
    ;;
  *)
    echo "Invalid bump type. Using patch."
    ((VERSION_PARTS[3]++))
    ;;
esac

NEW_VERSION="${VERSION_PARTS[1]}.${VERSION_PARTS[2]}.${VERSION_PARTS[3]}"
IFS='.' read -r -a VERSION_PARTS <<< "$NEW_VERSION"
if [ "${VERSION_PARTS[2]}" == "" ]; then
  NEW_VERSION="${NEW_VERSION}0"
fi
echo "$NEW_VERSION"
