import json
import boto3
import os
import urllib3

# Function to download dataset from Coinbase API
def download_dataset():
    url = "https://api.coinbase.com/v2/exchange-rates"
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    response_data = json.loads(response.data.decode("utf-8"))
    # print(json.dumps(response_data))
    if response.status == 200:
        return json.dumps(response_data)
    else:
        raise Exception(f"Failed to download dataset. Status Code: {response.status}")

# Function to store dataset in AWS S3
def store_to_s3(data):
    s3 = boto3.client('s3')
    bucket_name = os.environ['bucket']
    key = 'exchange_rates.json'

    s3.put_object(Body=data, Bucket=bucket_name, Key=key)
    return f"s3://{bucket_name}/{key}"

def lambda_handler(event, context):
    dataset = download_dataset()
    s3_url = store_to_s3(dataset)