import os
from flask import Flask, render_template
import boto3
import json

app = Flask(__name__)

@app.route('/')
def index():
    try:
        data = extract_czk_data()
        rate = data['CZK']
        return render_template('index.html', czk_rate=rate)
    except Exception as e:
        error_message = str(e)
        return render_template('error.html', error_message=error_message)

# Function to extract data relevant to CZK
def extract_czk_data():
    s3 = boto3.client('s3')
    response = s3.get_object(Bucket=os.environ['bucket'], Key='exchange_rates.json')
    data = json.loads(response['Body'].read().decode('utf-8'))
    czk_data = data.get('data', {}).get('rates', {}).get('CZK')
    if czk_data:
        return {'CZK': czk_data}
    else:
        raise Exception("CZK data not found in the dataset")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
